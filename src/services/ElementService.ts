/*
    If you thing "What the hell is that", I don't know too. This is hardcode to count max-height of the message container,
    cause using max-height: 100% doesn't work and messages stretch the container instead of overflow. So here I count
    exact value in px
*/
export const countMaxHeight = () => {
    const headerHeight = document.getElementsByTagName("header")[0].offsetHeight;
    const messageFormHeight = (document.getElementsByClassName('message-form')[0] as HTMLElement).offsetHeight;

    return  window.innerHeight - headerHeight - 20 - messageFormHeight;
}
