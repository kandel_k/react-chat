import {CallWebApi} from "./ApiService";

export const LoadMessages = async (url: string) => {
    return await CallWebApi(url);
}
