import React, {useEffect} from "react";
import PropTypes, {InferType} from 'prop-types'
import './style.scss'
import {ItemGroup} from "semantic-ui-react";
import Message from "../../components/Message/Message";
import {countMaxHeight} from "../../services/ElementService";

const MessageContainer: any = ({messages, currUser, deleteMessage, likeMessage, setUpdate}: InferType<typeof MessageContainer.propTypes>) => {

    useEffect(() => {
        let element = document.getElementsByClassName('message-container')[0] as HTMLElement;
        element.style.maxHeight = countMaxHeight() + 'px';
    }, [])

    return (
        <div className={'message-container'}>
            <ItemGroup className={'message-group'}>
                {messages.map((message: object, i: number) => {
                    return <Message key={i} message={message} currUser={currUser} deleteMessage={deleteMessage}
                                    likeMessage={likeMessage} setUpdate={setUpdate}/>
                })}
            </ItemGroup>
        </div>
    );
}

MessageContainer.propTypes = {
    messages: PropTypes.arrayOf(Object).isRequired,
    currUser: PropTypes.object.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    setUpdate: PropTypes.func.isRequired
}

export default MessageContainer;
