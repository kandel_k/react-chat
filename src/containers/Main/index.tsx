import React from "react";
import PropTypes, {InferProps} from 'prop-types'
import ChatField from "../ChatField";
import './style.scss'

const Main: any = ({messages, currUser, sendMessage, deleteMessage, likeMessage, updateMessage}: InferProps<typeof Main.propTypes>) => {
    return(
        <main>
            <div className='wrapper'>
                <ChatField messages={messages} currUser={currUser} sendMessage={sendMessage} deleteMessage={deleteMessage}
                           likeMessage={likeMessage} updateMessage={updateMessage}/>
            </div>
        </main>
    )
}

Main.propTypes = {
    messages: PropTypes.array.isRequired,
    currUser: PropTypes.object.isRequired,
    sendMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    updateMessage: PropTypes.func.isRequired
}

export default Main;
