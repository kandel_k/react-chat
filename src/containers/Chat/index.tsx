import React, {useState, useEffect} from 'react'
import Header from "../Header";
import './style.scss'
import Main from "../Main";
import {LoadMessages} from "../../services/MessageService";
import {Dimmer, Loader} from "semantic-ui-react";

export interface User {
    userId: string,
    avatar: string,
    user: string
}

export interface Message {
    id: string,
    userId: string,
    user: string,
    text: string,
    isLiked: boolean,
    createdAt: Date,
    editedAt: string
}

const Chat = () => {
    const [messages, setMessages] = useState([] as Message[]);
    const [users, setUsers] = useState([] as User[]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [lastMessage, setLastMessage] = useState(new Date());
    const [currUser, setCurrUser] = useState({userId: '', user: '', avatar: ''});

    const title = 'My room'

    const sendMessage = (newMessage: Message) => {
        const messageList = [...messages, newMessage];
        setLastMessage(newMessage.createdAt);
        setMessages(messageList);
    }

    const deleteMessage = (message: Message) => {
        const messageList = messages.filter(el => el.id !== message.id);
        setLastMessage(messageList[messageList.length-1].createdAt);
        setMessages(messageList);
    }

    const likeMessage = (message: Message) => {
        const messageList = [...messages];
        const index = messages.indexOf(message);
        messageList[index].isLiked = !messageList[index].isLiked;
        setMessages(messageList);
    }

    const updateMessage = (message: Message) => {
        const messageList = [...messages];
        messageList.forEach(v => {
            if (v.id === message.id) {
                v.text = message.text;
            }
        })
    }

    const loadData = async () => {
        const data: (User & Message)[] = await LoadMessages('https://edikdolynskyi.github.io/react_sources/messages.json');

        return data;
    }

    const loadMessages = (data:  Message[]) => {
         const messageList = data.map(el => {
            el.createdAt = new Date(el.createdAt);
            return el;
        })

        setLastMessage(messageList[messageList.length-1].createdAt);
        setMessages(messageList);
    }

    const loadUsers = (data: User[]) => {
        const userList = data.filter((v, i, a) => {
            return  a.findIndex(user => user.userId === v.userId) === i
        });
        // I hardcode this user as we don't have registration and just generating user is also not interesting
        const currUser: User = {userId: 'This should be generated id or username', user: '', avatar: ''};
        userList.push(currUser);

        setCurrUser(currUser);
        setUsers(userList);
    }

    useEffect(() => {
        if (!isLoaded) {
            loadData().then(data => {
                loadMessages(data);
                loadUsers(data);
                setIsLoaded(true);
            })
        }
    }, [isLoaded]);

    const showLoading = () => {
        return (
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    const showChat = () => {
        return (
            <div className={'container'}>
                <Header roomTitle={title} participants={users.length} messages={messages.length}
                        lastMessage={lastMessage}/>
                <Main messages={messages} currUser={currUser} sendMessage={sendMessage} deleteMessage={deleteMessage}
                      likeMessage={likeMessage} updateMessage={updateMessage}/>
            </div>
        );
    }

    return (
        (!isLoaded && showLoading()) || showChat()
    );
}

export default Chat;
