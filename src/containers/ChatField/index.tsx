import React, {useState} from "react";
import PropTypes, {InferProps} from 'prop-types'
import MessageForm from "../../components/SendMessageForm/MessageForm";
import './style.scss'
import MessageContainer from "../MessageContainer";
import {Message} from "../Chat";

const ChatField: any = ({messages, currUser, sendMessage, deleteMessage, likeMessage, updateMessage}: InferProps<typeof ChatField.propTypes>) => {
    const [updatedMessage, setUpdatedMessage] = useState({});

    const setUpdate = (message: Message) => {
        setUpdatedMessage(message);
    }

    /*
        In message container user choose a message to edit then it comes here to set update method. Then it goes to the
        message form. There user change the text of the letter and send it to update method and from here updated message
        goes to the main container chat
     */
    const update = (message: Message) => {
        if (message !== null) {
            if (message.text.length === 0) {
                deleteMessage(message);
                return;
            }
            updateMessage(message);
        }
        setUpdatedMessage({});
    }

    return (
        <div className={'chat-field'}>
            <MessageContainer messages={messages} currUser={currUser} deleteMessage={deleteMessage}
                              likeMessage={likeMessage} setUpdate={setUpdate}/>
            <MessageForm sendMessage={sendMessage} currUser={currUser} updatedMessage={updatedMessage} updateMessage={update}/>
        </div>
    );
}

ChatField.propTypes = {
    messages: PropTypes.array.isRequired,
    currUser: PropTypes.object.isRequired,
    sendMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    updateMessage: PropTypes.func.isRequired
}

export default ChatField;
