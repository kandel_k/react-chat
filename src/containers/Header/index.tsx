import React from 'react'
import PropTypes, { InferProps } from 'prop-types'

import './style.scss'
import HeaderLogo from "../../components/HeaderLogo";
import ChatHeader from "../../components/ChatHeader";

const Header: any = (
    {roomTitle, participants, messages, lastMessage}: InferProps<typeof Header.propTypes>
    ) => {

    return (
        <header>
            <div className='wrapper header'>
                <HeaderLogo/>
                <ChatHeader roomTitle={roomTitle} participants={participants} messages={messages} lastMessage={lastMessage} />
            </div>
        </header>
    );
}

Header.propTypes = {
    roomTitle: PropTypes.string.isRequired,
    participants: PropTypes.number.isRequired,
    messages: PropTypes.number.isRequired,
    lastMessage: PropTypes.instanceOf(Date).isRequired
}

export default Header;
