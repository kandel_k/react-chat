import React from "react";
import PropTypes, {InferType} from 'prop-types'
import {Item} from 'semantic-ui-react'
import './style.scss'
import Moment from 'react-moment'
import noAvatar from '../../resources/img/noavatar.png'

const Message: any = ({message, currUser, deleteMessage, likeMessage, setUpdate}: InferType<typeof Message.propTypes>) => {
    const isUpdatable = message.userId === currUser.userId;

    const deleteFunc = () => {
        deleteMessage(message);
    }

    const likeFunc = () => {
        likeMessage(message);
    }

    const update = () => {
        setUpdate(message);
    }

    return (
            <Item className={(isUpdatable &&'align-self-right message') || 'message'}>
                {!isUpdatable
                    && <Item.Image size={"tiny"} src={(message.avatar && message.avatar.length > 0) ? message.avatar : noAvatar} />}

                <Item.Content >
                    <Item.Header as='a'>{(message.user && message.user.length > 0) ? message.user : message.userId}</Item.Header>
                    <Item.Description>{message.text}</Item.Description>
                    <Item.Extra className={'user-menu'}>
                        {isUpdatable && <p onClick={update}>Edit</p>}
                        {isUpdatable && <p onClick={deleteFunc}>Delete</p>}
                        {(!isUpdatable && <p onClick={likeFunc}>{!message.isLiked ? 'Like' : 'Liked'}</p>)}
                        <Moment fromNow>{message.createdAt}
                    </Moment></Item.Extra>
                </Item.Content>
            </Item>
    );
}

Message.propTypes = {
    message: PropTypes.object.isRequired,
    currUser: PropTypes.object.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    setUpdate: PropTypes.func.isRequired
}

export default Message;
