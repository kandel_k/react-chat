import React, {useEffect, useState} from "react";
import PropTypes, {InferProps} from 'prop-types';
import {Form, Button, FormGroup, TextArea} from "semantic-ui-react";
import './style.scss'
import {Message} from "../../containers/Chat";

const MessageForm: any = ({sendMessage, currUser, updateMessage, updatedMessage}: InferProps<typeof MessageForm.propTypes>) => {
    const [text, setText] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [isUpdate, setIsUpdate] = useState(false);

    const send = () => {
        if (text.length === 0) {
            return;
        }
        setIsLoading(true);

        let message: Message = {
            // Here should be generated id from database. As we don't have it I wrote simple.
            id: '' + Math.random() * 100,
            userId: currUser.userId,
            user: currUser.user,
            text: text,
            isLiked: false,
            createdAt: new Date(),
            editedAt: ''
        }
        sendMessage(message);
        setText('');

        setIsLoading(false);
    }

    const update = () => {
        setIsLoading(true);

        updatedMessage.text = text;
        updatedMessage.editedAt = new Date();
        setText('');
        updateMessage(updatedMessage);

        setIsUpdate(false);
        setIsLoading(false);
    }

    const cancelUpdate = () => {
        setText('');
        setIsUpdate(false);
        updateMessage(null);
    }

    useEffect(() => {
        if (updatedMessage.id) {
            setIsUpdate(true);
            setText(updatedMessage.text);
        }
    }, [updatedMessage])

    const handleChange = (event: any) => setText(event.target.value);

    const handleKeyDown = (event: KeyboardEvent) => {
        if (event.key === 'Enter') {
            isUpdate ? update() : send();
            event.preventDefault();
        }
    }

    return (
        <Form className={'message-form'}>
            <FormGroup>
                <Form.Field control={TextArea} placeholder={'Type your message to send'} width={12} rows={2}
                            value={text} onChange={handleChange} onKeyDown={handleKeyDown}/>
                <Button loading={isLoading} color={"blue"} size={"medium"} onClick={isUpdate ? update : send}>
                    {isUpdate ? 'Update' : 'Send'}
                </Button>
                {isUpdate && <Button size={"medium"} onClick={cancelUpdate}>Cancel</Button>}
            </FormGroup>
        </Form>
    );
}

MessageForm.propTypes = {
    sendMessage: PropTypes.func.isRequired,
    currUser: PropTypes.object.isRequired,
    updatedMessage: PropTypes.object.isRequired,
    updateMessage: PropTypes.func.isRequired
}

export default MessageForm;
