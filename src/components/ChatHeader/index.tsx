import React from 'react'
import PropTypes, { InferProps } from 'prop-types'
import { Icon } from 'semantic-ui-react'
import './style.scss'
import Moment from "react-moment";

const ChatHeader: any =
    ({ roomTitle, participants, messages, lastMessage }: InferProps<typeof ChatHeader.propTypes>
    ) => {

    return (
        <div className='room_header'>
            <div className='room_info'>
                <p className='title'>{roomTitle}</p>
                <p><Icon name='users' /> {participants}</p>
                <p><Icon name='envelope outline' /> {messages}</p>
            </div>
            <p className='room_last-message'>Last sent  <Moment fromNow>{lastMessage}</Moment></p>
        </div>
    );
}

ChatHeader.propTypes = {
    roomTitle: PropTypes.string.isRequired,
    participants: PropTypes.number.isRequired,
    messages: PropTypes.number.isRequired,
    lastMessage: PropTypes.instanceOf(Date).isRequired
}

export default ChatHeader;
